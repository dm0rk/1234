<?php

function reverseString($str) {
 // Тут функция будет выводить последовательность символов в обратном порядке.
 $result = "";
 for ($i = mb_strlen($str, "UTF-8"); $i >= 0; $i--){
 $result .= mb_substr($str, $i, 1, "UTF-8");
    }
    return $result;
}
